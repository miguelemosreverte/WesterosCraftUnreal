// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved. 

using UnrealBuildTool;
using System.IO;
using System;

namespace UnrealBuildTool.Rules
{
	public class AnvilGen : ModuleRules
	{
        protected string ModulePath
        {
            get { return ModuleDirectory; }
        }

        protected string ThirdPartyPath
        {
            get { return Path.GetFullPath(Path.Combine(ModulePath, "..", "..", "ThirdParty")); }
        }

		public AnvilGen(TargetInfo Target)
		{
            MinFilesUsingPrecompiledHeaderOverride = 1;
            bFasterWithoutUnity = true;

            PrivateIncludePaths.AddRange(new string[]
            {
                Path.Combine("AnvilGen", "Private")
            });

            PublicIncludePaths.AddRange(new string[]
            {
                Path.Combine("AnvilGen", "Public")
            });

			PublicDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					"Engine",
					"BrickGrid"
				}
			);

            LoadLibmcmap(Target);
		}

        private bool LoadLibmcmap(TargetInfo Target)
        {
            if ((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32))
            {
                string LibmcmapPath = Path.Combine(ThirdPartyPath, "libmcmap", "Libraries");
                string PlatformString = (Target.Platform == UnrealTargetPlatform.Win64) ? "x64" : "x86";

                PublicAdditionalLibraries.Add(Path.Combine(LibmcmapPath, "libmcmap." + PlatformString + ".lib"));
                PublicAdditionalLibraries.Add(Path.Combine(LibmcmapPath, "zlibstat." + PlatformString + ".lib"));

                Definitions.Add(string.Format("WITH_LIBMCMAP=1"));
                return true;
            }

            Definitions.Add(string.Format("WITH_LIBMCMAP=0"));
            return false;
        }
	}
}
