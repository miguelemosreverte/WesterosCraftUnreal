// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved. 

#pragma once

#include "CoreUObject.h"
#include "Engine.h"
#include "BrickGridComponent.h"
#include "AnvilGen.generated.h"

USTRUCT(BlueprintType)
struct FAnvilGenParameters
{
	GENERATED_USTRUCT_BODY()

	// The name of the world file to load.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anvil Generation")
	FString WorldName;

	FAnvilGenParameters()
		: WorldName("TestWorld")
	{}
};

UCLASS()
class UAnvilGen : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()
public:

	UFUNCTION(BlueprintCallable, Category = "Anvil Generation")
	static ANVILGEN_API void InitRegion(const FAnvilGenParameters& Parameters, class UBrickGridComponent* Grid, const struct FInt3& RegionCoordinates);
	
	UFUNCTION(BlueprintCallable, Category = "Anvil Generation")
	static ANVILGEN_API void TestMessage(const FAnvilGenParameters& Parameters, class UBrickGridComponent* Grid);
};