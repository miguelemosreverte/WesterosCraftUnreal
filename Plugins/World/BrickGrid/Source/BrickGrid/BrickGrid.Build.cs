// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

namespace UnrealBuildTool.Rules
{
	public class BrickGrid : ModuleRules
	{
        public BrickGrid(TargetInfo Target)
		{
            MinFilesUsingPrecompiledHeaderOverride = 1;
            bFasterWithoutUnity = true;

			PrivateIncludePaths.Add("BrickGrid/Private");
			PublicDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					"Engine",
					"RenderCore",
					"ShaderCore",
					"RHI"
				}
				);
		}
	}
}
