﻿# **Overview**

WesterosCraftUnreal is a derivation of Andrew Scheidecker's [BrickGrid plugin](https://github.com/AndrewScheidecker/BrickGame/tree/master/Plugins/BrickGrid/Source/BrickGrid), meant to demonstrate the parsing of Minecraft's native NBT format directly to Unreal Engine 4.

This project is built for the purposes of the [WesterosCraft](http://www.westeroscraft.com) server, though most of the functionality is derived from UE4 plugins. Therefore, a selection of these plugins may be used universally for independent projects without modification of the source code.

![ScreenShot](http://i.imgur.com/RhqDjz6.png)

# **Engine modification**

WesterosCraftUnreal is built with a slightly modified branch of Unreal Engine 4.11. The code for the branch is [here](https://github.com/AndrewScheidecker/UnrealEngine/tree/BrickGame-4.11). To use this branch to link against WesterosCraftUnreal, clone it locally, and follow the [instructions](https://github.com/AndrewScheidecker/UnrealEngine/blob/BrickGame-4.11/README.md) to register the branch for use by the "Generate Visual Studio Projects" command.

The changes are:

* Light propagation volume is hacked to ignore the ambient occlusion values computed for the bricks, which will only apply to the ambient cubemap.

* The way the renderer determines which static elements of a primitive are drawn is modified to allow WesterosCraftUnreal to cull back faces for entire chunks before sending anything to the GPU. WesterosCraftUnreal will compile without this change, but it will not benefit from the early backface culling.

# **License**
### WesterosCraftUnreal License ###

WesterosCraftUnreal and corresponding plugins (AnvilGen) copyright (c) 2015, WesterosCraft.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

* Neither the name of WesterosCraftUnreal nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL WESTEROSCRAFT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### BrickGame License ###

BrickGame and corresponding plugins (BrickGrid, BrickTerrainGeneration, ConsoleAPI, Simplex Noise, Super Loop Library) copyright (c) 2014-2015, Andrew Scheidecker.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

* Neither the name of BrickGame nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ANDREW SCHEIDECKER BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### Libmcmap License ###

Copyright 2013-2015 Peter Markley <quartz@malexmedia.net>.
Distributed under the terms of the Lesser GNU General Public License.

Libmcmap & libnbt are free software: you can redistribute them and/or modify them under the terms of the Lesser GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Libmcmap & libnbt are distributed in the hope that they will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License along with libmcmap & libnbt. If not, see < http://www.gnu.org/licenses/ >.

Unreal® is a trademark or registered trademark of Epic Games, Inc. in the United States of America and elsewhere

Unreal® Engine, Copyright 1998 – 2014, Epic Games, Inc.  All rights reserved.